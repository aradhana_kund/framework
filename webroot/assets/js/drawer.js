function toggleRotateArrow() {
  let icon = document.getElementById("drawer-arrow");
  if (icon.classList.contains("fa-angle-down")) {
    icon.classList.remove("fa-angle-down");
    icon.classList.add("fa-angle-up");
  } else {
    icon.classList.remove("fa-angle-up");
    icon.classList.add("fa-angle-down");
  }
}

function openSlideMenu() {
  if (window.innerWidth < 600)
    document.getElementById("side-menu").style.width = "100%";
  else document.getElementById("side-menu").style.width = "25rem";
  document.getElementById("faded-bg").classList.remove("d-none");
}

function closeSlideMenu() {
  document.getElementById("faded-bg").classList.add("d-none");
  document.getElementById("side-menu").style.width = "0";
}
