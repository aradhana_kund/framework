// Set default margins, width and height
var margin = {top: 10, right: 30, bottom: 150, left: 100};
var width = 800 - margin.left - margin.right;
var height = 600 - margin.top - margin.bottom;

// Add SVG to line chart container and append a group to the same
var svg = d3.select("#line-chart")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


// Format data
var categories = [];
// For all types of key except period(x-axis variable), format them
for (var key in data[0]) {
     if (key != 'period') {
         var obj = {
             category: key,  // categorical variable
             values: data.map(function(d) {
                 return {period: d.period, yValue: d[key]};
             })
         };
         categories.push(obj);
     }
}


// Define X-scale
var xScale = d3.scaleBand()
    .domain(data.map(function(d) {
        return d.period;
    }))
    .range([ 0, width ])
    .paddingInner(1.0)
    .paddingOuter(0.2);

// Append X-axis to the Line Chart
xAxis = svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(xScale));

// Rotate labels separately
xAxis.selectAll('text')
    .attr('y', 10)
	.attr('x', -5)
	.attr('text-anchor', 'end')
	.attr('transform', 'rotate(-50)');

// Custom Invert function for Band Scale
function scaleBandInvert(scale) {
    var domain = scale.domain();
    var paddingOuter = scale(domain[0]);
    var eachBand = scale.step();
    
    return function (value) {
        var index = Math.floor(((value - paddingOuter) / eachBand));
        return domain[Math.max(0,Math.min(index, domain.length-1))];
    }
}

// Define Y-scale
var yScale = d3.scaleLinear()
    .domain([
        d3.min(categories, function(c) { return d3.min(c.values, function(d) { return d.yValue; }); }),
        d3.max(categories, function(c) { return d3.max(c.values, function(d) { return d.yValue; }); })
    ])
    .range([ height, 0 ]);

// Append Y-axis to the Line Chart
yAxis = svg.append("g")
    .call(d3.axisLeft(yScale))
    .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "0.71em")
        .attr("fill", "#000")
        .text("Number of Artifacts");


// Define Color Scale
var colorScale = d3.scaleOrdinal(d3.schemeCategory10)
    .domain(categories.map(function(c) { 
        return c.category; 
    }));


// Add Legend to the Line Chart
var legend = svg.append("g")
    .attr('transform', 'translate(' + width + ', 0)');

var i = 0;
for (var key in data[0]) {
     if (key != 'period') {
         var legendRow = legend.append('g')
            .attr('transform', 'translate(0, ' + (i*20) + ')');

        legendRow.append('rect')
            .attr('height', 10)
            .attr('width', 10)
            .attr('fill', colorScale(key));

        legendRow.append('text')
            .attr('x', -10)
            .attr('y', 10)
            .attr('text-anchor', 'end')
            .style('text-transform', 'capitalize')
            .text(key);
         
         i++;
     }
}


// Add a clipPath: everything out of this area won't be drawn.
var clip = svg.append("defs").append("svg:clipPath")
    .attr("id", "clip")
    .append("svg:rect")
    .attr("width", width )
    .attr("height", height )
    .attr("x", 0)
    .attr("y", 0);


// Define brush restricted to X-axis
var brush = d3.brushX()                   // Add the brush feature using the d3.brush function
    .extent( [ [0,0], [width,height] ] )  // Initialise the brush area: start at 0,0 and finishes at width,height: it means the whole graph area is selected
    .on("end", updateChart)               // Each time the brush selection changes, trigger the 'updateChart' function

// Define Line
var line = d3.line()
//    .curve(d3.curveBasis)
    .x(function(d) { return xScale(d.period); })
    .y(function(d) { return yScale(d.yValue); });

// Add Line categorized by categorical variable
var category = svg.selectAll(".category")
    .data(categories)
    .enter().append("g")
        .attr("clip-path", "url(#clip)")
        .attr("class", "category");

category.append("path")
    .attr("class", "line")
    .attr("fill", "none")
    .attr("d", function(d) { return line(d.values); })
    .style("stroke", function(d) { return colorScale(d.category); })
    .attr("stroke-width", 2.5);

//// Append text after the last point
//category.append("text")
//    .datum(function(d) { 
//        return {category: d.category, value: d.values[d.values.length - 1]}; 
//    })
//    .attr("transform", function(d) { return "translate(" + xScale(d.value.period) + "," + yScale(d.value.yValue) + ")"; })
//    .attr("x", 3)
//    .attr("dy", "0.35em")
//    .style("font", "10px sans-serif")
//    .text(function(d) { return d.category; });

// Add the brushing to the SVG
svg.append("g")
    .attr("class", "brush")
        .call(brush);


// Update the chart for given boundaries
function updateChart() {
    // Get the selected boundaries
    extent = d3.event.selection
        
    var start = scaleBandInvert(xScale)(extent[0]);
    var end = scaleBandInvert(xScale)(extent[1]);
    var count = false;
        
    var filteredData = [];
    data.forEach(function(d) {
        if (d.period == start)
            count = true;
            
        if (count)
            filteredData.push(d);
            
        if (d.period == end)
            count = false;                
    });
        
    xScale.domain(filteredData.map(function(d) {
        return d.period;
    }))

    // Remove the grey brush area as soon as the selection has been done
    category.select(".brush").call(brush.move, null);

    // Update axis and line position
    xAxis.transition().duration(1000).call(d3.axisBottom(xScale));
    
    // Format filteredData
    var filteredCategories = [];
    // For all types of key except period(x-axis variable), format them
    for (var key in filteredData[0]) {
         if (key != 'period') {
             var obj = {
                 category: key,  // categorical variable
                 values: filteredData.map(function(d) {
                     return {period: d.period, yValue: d[key]};
                 })
             };
             filteredCategories.push(obj);
         }
    }
    
    category.data(filteredCategories)
        .select('.line')
        .transition()
        .duration(1000)
        .attr("d", function(d) { return line(d.values); })
        .style("stroke", function(d) { return colorScale(d.category); });
}

// Reinitialize the chart on double click
svg.on("dblclick", function() {
    xScale.domain(data.map(function(d) {
        return d.period;
    }))
    
    xAxis.transition()
        .call(d3.axisBottom(xScale))
        .selectAll('text')
            .attr('y', 10)
            .attr('x', -5)
            .attr('text-anchor', 'end')
            .attr('transform', 'rotate(-50)');
    
    category.data(categories)
        .select('.line')
        .transition()
        .attr("d", function(d) { return line(d.values); })
        .style("stroke", function(d) { return colorScale(d.category); });
});