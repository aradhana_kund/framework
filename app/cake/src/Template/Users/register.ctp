<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<main class="row justify-content-md-center">

    <div class="login-box">
        <div class="capital-heading text-center">
            <h1 class="display-4 header-txt">Register</h1>
        </div>
        <?= $this->Flash->render() ?>

        <?= $this->Form->create() ?>
            <?= $this->Form->control('email', ['class' => 'col-12 input-background-child-md']) ?>
            <?php if (isset($errors['email'])):?>
                <?php foreach ($errors['email'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('username', ['class' => 'col-12 input-background-child-md']) ?>
            <?php if (isset($errors['username'])):?>
                <?php foreach ($errors['username'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('password', ['class' => 'col-12 input-background-child-md']) ?>
            <?php if (isset($errors['password'])):?>
                <?php foreach ($errors['password'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>
            <?php if (isset($badPassword) && strlen($_POST['password']) ):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    Please use a more secure password. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            <?php endif;?>

            <?= $this->Form->control('password_confirm', [
                'class' => 'col-12 input-background-child-md',
                'label'=>'Confirm Password ',
                'type'=>'password'
            ]) ?>
            <?php if (isset($errors['password_confirm'])):?>
                <?php foreach ($errors['password_confirm'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <div class="userloginbuttoncenter">
                 <?= $this->Form->submit('Submit', ['class' => 'btn cdli-btn-blue']) ?>
            </div> 

        <?= $this->Form->end() ?>
    </div>

</main>
