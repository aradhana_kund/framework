<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting $posting
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($posting) ?>
            <legend class="capital-heading"><?= __('Add Posting') ?></legend>
            <?php
                echo $this->Form->control('posting_type_id', ['options' => $postingTypes, 'empty' => true]);
                echo $this->Form->control('slug');
                echo $this->Form->control('published');
                echo $this->Form->control('title');
                echo $this->Form->control('body');
                echo $this->Form->control('lang');
                echo $this->Form->control('created_by');
                echo $this->Form->control('modified_by');
                echo $this->Form->control('publish_start', ['empty' => true]);
                echo $this->Form->control('publish_end', ['empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Postings'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Posting Types'), ['controller' => 'PostingTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting Type'), ['controller' => 'PostingTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
