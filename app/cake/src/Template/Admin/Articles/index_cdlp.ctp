<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */
?>

<h3 class="display-4 pt-3"><?= __('Cuneiform Digital Library Preprints') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th>No.</th>
            <th>Author</th>
            <th>Title</th>
            <th>Date</th>
            <th>File</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody align="left" class="journals-view-table">
        <?php $i=1; foreach ($cdlp as $artcile) {  ?>
            <tr>
                <td width="5%"><?= $artcile['serial']; ?></td>
                <td width="25%"><?= $artcile['authors']; ?></td>
                <td width="40%"><?= $artcile['title']; ?></td>
                <td width="15%"><?= $artcile['created']; ?></td>
                <td width="15%"><a href="cdlp/view/<?= $artcile['article_id']; ?>">pdf</a></td>
                <td width="15%">
                    <a href="cdlp/view/<?= $artcile['article_id']; ?>"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                    <a href="edit/CDLP/<?= $artcile['article_id']; ?>"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>                    
                   
                </td>
            </tr>
       <?php } ?>
    </tbody>
</table>


<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
    </ul>
</div>
<div class="boxed mx-0">
    <?php if (empty($journal->publications)): ?>
        <div class="capital-heading"><?= __('No Related Publications') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Publications') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Bibtexkey') ?></th>
                <th scope="col"><?= __('Year') ?></th>
                <th scope="col"><?= __('Entry Type Id') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Annote') ?></th>
                <th scope="col"><?= __('Book Title') ?></th>
                <th scope="col"><?= __('Chapter') ?></th>
                <th scope="col"><?= __('Crossref') ?></th>
                <th scope="col"><?= __('Edition') ?></th>
                <th scope="col"><?= __('Editor') ?></th>
                <th scope="col"><?= __('How Published') ?></th>
                <th scope="col"><?= __('Institution') ?></th>
                <th scope="col"><?= __('Journal Id') ?></th>
                <th scope="col"><?= __('Month') ?></th>
                <th scope="col"><?= __('Note') ?></th>
                <th scope="col"><?= __('Number') ?></th>
                <th scope="col"><?= __('Organization') ?></th>
                <th scope="col"><?= __('Pages') ?></th>
                <th scope="col"><?= __('Publisher') ?></th>
                <th scope="col"><?= __('School') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Volume') ?></th>
                <th scope="col"><?= __('Publication History') ?></th>
                <th scope="col"><?= __('Abbreviation Id') ?></th>
                <th scope="col"><?= __('Series') ?></th>
                <th scope="col"><?= __('Oclc') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <!-- <th scope="col"><?= __('Actions') ?></th> -->
            </thead>
            <tbody>
                <?php foreach ($journal->publications as $publications): ?>
                <tr>
                    <td><?= h($publications->id) ?></td>
                    <td><?= h($publications->bibtexkey) ?></td>
                    <td><?= h($publications->year) ?></td>
                    <td><?= h($publications->entry_type_id) ?></td>
                    <td><?= h($publications->address) ?></td>
                    <td><?= h($publications->annote) ?></td>
                    <td><?= h($publications->book_title) ?></td>
                    <td><?= h($publications->chapter) ?></td>
                    <td><?= h($publications->crossref) ?></td>
                    <td><?= h($publications->edition) ?></td>
                    <td><?= h($publications->editor) ?></td>
                    <td><?= h($publications->how_published) ?></td>
                    <td><?= h($publications->institution) ?></td>
                    <td><?= h($publications->journal_id) ?></td>
                    <td><?= h($publications->month) ?></td>
                    <td><?= h($publications->note) ?></td>
                    <td><?= h($publications->number) ?></td>
                    <td><?= h($publications->organization) ?></td>
                    <td><?= h($publications->pages) ?></td>
                    <td><?= h($publications->publisher) ?></td>
                    <td><?= h($publications->school) ?></td>
                    <td><?= h($publications->title) ?></td>
                    <td><?= h($publications->volume) ?></td>
                    <td><?= h($publications->publication_history) ?></td>
                    <td><?= h($publications->abbreviation_id) ?></td>
                    <td><?= h($publications->series) ?></td>
                    <td><?= h($publications->oclc) ?></td>
                    <td><?= h($publications->designation) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Publications', 'action' => 'view', $publications->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Publications', 'action' => 'edit', $publications->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Publications', 'action' => 'delete', $publications->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $publications->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

