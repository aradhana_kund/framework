<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('User Profile') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Username') ?></th>
                    <td><?= h($user['username']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= h($user['email']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($user['created_at']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Admin') ?></th>
                    <td><?= in_array(1, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Editor') ?></th>
                    <td><?= in_array(2, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can Download HD Images') ?></th>
                    <td><?= in_array(3, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View Private Artifacts') ?></th>
                    <td><?= in_array(4, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View Private Inscriptions') ?></th>
                    <td><?= in_array(5, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can Edit Transliterations') ?></th>
                    <td><?= in_array(6, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View Private Images') ?></th>
                    <td><?= in_array(7, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View CDLI Tablet Manager') ?></th>
                    <td><?= in_array(8, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Can View CDLI Forums') ?></th>
                    <td><?= in_array(9, $user['roles']) ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Active') ?></th>
                    <td><?= $user['active'] ? __('Yes') : __('No'); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Flash->render() ?>
        <?= $this->Html->link(__('Edit this profile'), '/admin/users/edit/'.$user['username'],
            ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('View all Users'), [
                'action' => 'view'
            ], [
                'class' => 'btn-action'
            ]) ?>
    </div>

</div>
