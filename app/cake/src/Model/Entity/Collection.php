<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Collection Entity
 *
 * @property int $id
 * @property string $collection
 * @property string|null $geo_coordinates
 * @property int|null $slug
 * @property bool|null $is_private
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Collection extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'collection' => true,
        'geo_coordinates' => true,
        'slug' => true,
        'is_private' => true,
        'artifacts' => true
    ];

    public function getCidocCrm()
    {
        [$lat, $long] = empty($this->geo_coordinates)
            ? [null, null]
            : explode(', ', $this->geo_coordinates);

        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E78_Collection',
            'crm:P1_is_identified_by' => [
                '@type' => 'crm:E41_Appellation',
                'rdfs:label' => $this->collection
            ],
            'crm:P46_is_composed_of' => self::getEntities($this->artifacts),
            'geo:lat' => $lat,
            'geo:long' => $long
            // TODO slug
        ];
    }
}
