<?php
namespace App\View;

use App\Model\Entity\Inscription;
use Cake\View\SerializedView;

class CdliConllView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'cdli-conll';

    protected function _serialize($serialize)
    {
        $inscription = $this->_dataToSerialize($serialize);

        if (is_array($inscription)) {
            $inscription = $inscription[0];
        }

        if ($inscription instanceof Inscription) {
            $this->response = $this->response->withDownload(
                $inscription->artifact->getCdliNumber() . '.conll'
            );
            return $inscription->annotation;
        } else {
            return null;
        }
    }
}
