<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false
        ]);
        $this->loadComponent('RestPaginator');
        $this->loadComponent('Flash');

        // Component for Authentication and Authorization. Default login method is Users/index. All fields is viewable except Users/view.
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authError' => ' You are not authorized to access that location.',
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password']
                ]
            ],
            'loginAction' => [
                'prefix'=> false,
                'controller'=>'Users',
                'action'=>'login'
            ],
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $session = $this->getRequest()->getSession();

        $session_user = $session->read('user');

        // Check if user session details exists.
        if (!is_null($session_user)) {
            // Check if present Controller is TwoFactor
            if ($this->getRequest()['controller'] === 'Twofactor') {
                // Check if session 'user' created is not more than 2 minutes.
                $session_user_created = strtotime($session_user['2fa_session_created']);
                $time_now = strtotime(Time::now());
                $time_diff = $time_now - $session_user_created;

                // If time differece is more then 2 minutes, delete the session 'user'
                if ($time_diff > 120) {
                    $session->delete('user');
                    $this->Flash->error(__("Please login or register again, two-step set-up and verification should be resolved in under 120 seconds."));
                }
            } else {
                if ($session_user['type'] === 'login') {
                    $this->Flash->error(__("Login failed. Please complete the two-factor authentication to log in."));
                } else {
                    $this->Flash->error(__("Registration incomplete. To register, please fill the register form again and complete the two-factor authentication setup."));
                }
                $session->delete('user');
            }
        }

        $session_forgot = $session->read('forgotPassword');

        // Check if forgot_password session exists
        if (!is_null($session_forgot)) {
            if ((Time::now())->toUnixString() > $session_forgot['expired_at']) {
                $session->delete('forgotPassword');
            } else {
                if ($this->getRequest()['controller'] !== 'Forgot' && $session_forgot['2fa'] == 1) {
                    $session->write('forgotPassword.2fa', false);
                    $this->Flash->error("To set up new password, you will have to again verify 2FA.");
                }
            }
        }

        // Initialize Search Settings
        if (is_null($session->read('searchSettings'))) {
            $this->loadComponent('GeneralFunctions')->initializeSearchSettings();
        }
    }

    /***
     * Check authorization.
     *
     * Users
     *
     * - see user profile
     * - edit password
     * - edit user info
     * - edit email (with email check)
     * (that's all for now)
     *
     * Editors (all previous rights)
     *
     * - see and see the forum
     * - add, edit artifacts
     * - add, edit transliterations
     * - add, edit linguistic annotations
     *
     * Admins ( all previous rights)
     *
     * - add, edit, delete users
     * - delete artifacts
     * - add, edit, delete most entities related to artifacts (eg collections, materials, etc)
     * - add, edit, delete postings
     * - add, edit, delete journal articles
     */

    public function isAuthorized($user = null)
    {
        // Only admins can access admin functions
        if ($this->request->getParam('prefix') === 'admin') {
            // Roles which can access /admin
            $checkIfRolesExists = $this->loadComponent('GeneralFunctions')->checkIfRolesExists([1, 2, 3, 4, 5, 6, 7, 8, 9]);
            return $checkIfRolesExists;
        } else {
            // Default: anonymous visitors can vew all non-admin prefixed pages
            return true;
        }
    }
}
