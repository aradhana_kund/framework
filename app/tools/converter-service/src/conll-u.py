import sys
import os
sys.path.append('../CoNLL-U')

from cdliconll2conllu.converter import CdliCoNLLtoCoNLLUConverter

# adapted from converter.py to support STDIN
def main():
    stub = os.path.abspath('stub')
    converter = CdliCoNLLtoCoNLLUConverter(stub, stub, False)

    inputLines = list()
    for line in sys.stdin:
        line = line.strip()
        if (len(line) == 0):
            continue

        if line[0] != '#':
            line = line.split("\t")
            inputLines.append(line)
        else:
            converter.headerLines.append(line)

    try:
        converter.convertCDLICoNLLtoCoNLLU(inputLines)
    except:
        pass

    print(converter.headerLines[0])
    print('# ' + '\t'.join(converter.cl.conllUFields))

    for line in converter.outputLines:
        print('\t'.join(line))

if __name__ == "__main__":
    main()
