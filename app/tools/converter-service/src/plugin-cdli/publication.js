module.exports.convertToTarget = function (publication) {
    const output = {
        type: publication.entry_type ? publication.entry_type.label : 'misc',
        label: publication.bibtexkey || `CDLI:publication-${publication.id}`,
        properties: {}
    }

    publication.author = publication.authors
    publication.booktitle = publication.book_title
    publication.editor = publication.editors
    publication.howpublished = publication.how_published

    for (const prop in publication) {
        if (publication[prop] === null || publication[prop].length === 0) {
            continue
        }

        output.properties[prop] = (function (value) {
            switch (prop) {
                case 'journal':
                    return value.journal
                case 'author':
                case 'editor':
                    return value.map(author => author.author).join(' and ')
                default:
                    return value
            }
        })(publication[prop])
    }

    return output
}
